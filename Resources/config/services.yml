parameters:
    imatic_view.menu.main.class: Imatic\Bundle\ViewBundle\Menu\MainMenuBuilder

services:
    # Menu
    imatic_view.menu.provider_container:
        class: Imatic\Bundle\ViewBundle\Menu\ContainerProvider
        arguments:
          - '@service_container'
        tags:
          - { name: knp_menu.provider }

    imatic_view.menu.factory:
        class: Imatic\Bundle\ViewBundle\Menu\Factory
        arguments:
            - '@knp_menu.factory'
            - '@imatic_view.menu.factory.array_loader'
            - '@imatic_view.menu.factory.node_loader'

    imatic_view.menu.factory.array_loader:
        class: Knp\Menu\Loader\ArrayLoader
        public: false
        arguments: [ '@knp_menu.factory' ]

    imatic_view.menu.factory.node_loader:
        class: Knp\Menu\Loader\NodeLoader
        public: false
        arguments: [ '@knp_menu.factory' ]

    imatic_view.menu.helper:
        class: Imatic\Bundle\ViewBundle\Menu\Helper
        arguments: [ '@translator', '@?security.authorization_checker', '@?security.token_storage' ]

    imatic_view.menu.main:
        class: '%imatic_view.menu.main.class%'
        arguments: [ '@imatic_view.menu.provider_container' ]
        tags:
            - { name: imatic_view.menu, alias: imatic.main }
            - { name: imatic_view.menu, alias: imatic.main_side, method: getSideMenu }

    imatic_view.menu.demo:
        class: Imatic\Bundle\ViewBundle\Menu\DemoMenuBuilder
        tags:
            - { name: imatic_view.menu, alias: imatic.demo }
            - { name: imatic_view.menu, alias: imatic.demo_normal, method: getNormalMenu }
            - { name: imatic_view.menu, alias: imatic.demo_sub, method: getSubMenu }

    # Twig extensions
    imatic_view.twig.extension.menu:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\MenuExtension
        arguments: [ '@imatic_view.menu.factory', '@knp_menu.renderer.twig' ]
        lazy: true
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.url:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\UrlExtension
        arguments:
            - '@imatic_view.templating.helper.url'
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.html:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\HtmlExtension
        arguments:
            - '@imatic_view.templating.helper.html'
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.example:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\ExampleExtension
        arguments:
            - '@templating.name_parser'
            - '@templating.locator'

        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.component:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\ComponentExtension
        arguments:
            - '@imatic_view.templating.helper.grid'
            - '@imatic_view.templating.helper.show'
            - '@imatic_view.templating.helper.action'
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.layout:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\LayoutExtension
        arguments:
            - '@imatic_view.templating.helper.layout'
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.format:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\FormatExtension
        lazy: true
        arguments:
            - '@imatic_view.templating.helper.format'
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.condition:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\ConditionExtension
        arguments:
            - '@imatic_view.templating.helper.condition'
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.type:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\TypeExtension
        tags:
            - { name: 'twig.extension' }

    imatic_view.twig.extension.resource:
        class: Imatic\Bundle\ViewBundle\Twig\Extension\ResourceExtension
        arguments:
            - '@imatic_view.templating.helper.resource'
        tags:
            - { name: 'twig.extension' }

    # Twig loaders
    imatic_view.twig.loader.remote:
        class: Imatic\Bundle\ViewBundle\Twig\Loader\RemoteLoader
        arguments:
            - '@service_container'
        tags:
            - { name: 'twig.loader' }

    # Formatters
    imatic_view.twig.extension.format.intl:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Format\IntlFormatter
        tags:
            - { name: imatic_view.formatter, alias: time }
            - { name: imatic_view.formatter, alias: date }
            - { name: imatic_view.formatter, alias: datetime }
            - { name: imatic_view.formatter, alias: number }

    imatic_view.twig.extension.format.common:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Format\CommonFormatter
        arguments:
            - '@translator'
        tags:
            - { name: imatic_view.formatter, alias: url, is_safe: html }
            - { name: imatic_view.formatter, alias: phone, is_safe: html }
            - { name: imatic_view.formatter, alias: email, is_safe: html }
            - { name: imatic_view.formatter, alias: text, is_safe: html }
            - { name: imatic_view.formatter, alias: html, is_safe: html }
            - { name: imatic_view.formatter, alias: boolean, is_safe: html }
            - { name: imatic_view.formatter, alias: link, is_safe: html }
            - { name: imatic_view.formatter, alias: filesize }
            - { name: imatic_view.formatter, alias: translatable }

    # Templating Helpers
    imatic_view.templating.helper.format:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Format\FormatHelper
        arguments:
            - '@service_container'

    imatic_view.templating.helper.grid:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Grid\GridHelper

    imatic_view.templating.helper.show:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Show\ShowHelper

    imatic_view.templating.helper.layout:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Layout\LayoutHelper
        arguments:
            - '@request_stack'

    imatic_view.templating.helper.action:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Action\ActionHelper
        arguments:
            - '@router'

    imatic_view.templating.helper.condition:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Condition\ConditionHelper
        arguments:
            - '@security.token_storage'
            - '@security.authorization_checker'
            - '@imatic_view.templating.helper.layout'

    imatic_view.templating.helper.url:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Html\UrlHelper

    imatic_view.templating.helper.html:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Html\HtmlHelper

    imatic_view.templating.helper.resource:
        class: Imatic\Bundle\ViewBundle\Templating\Helper\Resource\ResourceHelper
        arguments:
            - '@translator'
            - '@security.authorization_checker'

    # Event listners
    imatic_view.event_listener.kernel_subscriber:
        class: Imatic\Bundle\ViewBundle\EventListener\KernelSubscriber
        arguments:
            - '@imatic_view.templating.helper.layout'
            - '@translator'
            - '%kernel.debug%'
        tags:
            - { name: kernel.event_subscriber }
